import 'package:flutter/material.dart';
import 'package:friendly_chat/ui/friendly_chat.dart';
import 'package:flutter/cupertino.dart';

void main() {
  runApp(
    FriendlyChatApp(),
  );
}
